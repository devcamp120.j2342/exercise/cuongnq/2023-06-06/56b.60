package com.devcamp.customervisit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomervisitApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomervisitApplication.class, args);
	}

}
