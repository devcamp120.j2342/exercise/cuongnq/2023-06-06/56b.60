package com.devcamp.customervisit.controller;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.Customer;
import com.devcamp.model.Visit;
@RestController
@RequestMapping("/")
@CrossOrigin
public class controlllerVisit {
    @GetMapping("/visits")
    public ArrayList<Visit> getVisits(){
        ArrayList<Visit> Visits = new ArrayList<Visit>();
        Customer customer1 = new Customer("cuong");
        Customer customer2 = new Customer("phuong");
        Customer customer3 = new Customer("sang");


        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());


        Visit visit1 = new Visit(customer3, Date.valueOf(LocalDate.now()));
        Visit visit2 = new Visit(customer2, Date.valueOf(LocalDate.now()));
        Visit visit3 = new Visit(customer1, Date.valueOf(LocalDate.now()));


        System.out.println(visit1.toString());
        System.out.println(visit2.toString());
        System.out.println(visit3.toString());

        Visits.add(visit1);
        Visits.add(visit2);
        Visits.add(visit3);
        return Visits;
    }
}
