package com.devcamp.model;

import java.sql.Date;

public class Visit {
    private Customer customer;
    private Date date;
    private double serviceExpence;
    private double productExpence;


    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }
    public double getServiceExpence() {
        return serviceExpence;
    }
    public void setServiceExpence(double serviceExpence) {
        this.serviceExpence = serviceExpence;
    }
    public double getProductExpence() {
        return productExpence;
    }
    public void setProductExpence(double productExpence) {
        this.productExpence = productExpence;
    }

    public String geName(){
        return this.customer.getName();
    }

    public double getTotalExpence(){
        return this.serviceExpence+this.productExpence;
    }
    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", serviceExpence=" + serviceExpence
                + ", productExpence=" + productExpence + "]";
    }

}
